package com.app.common.repositories;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import com.app.common.entites.PlaylistSongEntity;
import com.app.common.entites.SongEntity;


public class PlaylistSongRepository extends BaseRepositoryImpl<PlaylistSongEntity> {

	
	public PlaylistSongRepository(String filePath, Class<PlaylistSongEntity> entityType) {
		super(filePath, entityType);
		
	}

	private List<Integer> getSongsIdByPlaylistId(int playlistId){
		List<Integer> songsIds =  new ArrayList<Integer>();
		Path path = Paths.get(filePath);
		
 		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				PlaylistSongEntity playlistSong =  new PlaylistSongEntity();
				playlistSong.setId(Integer.parseInt(value));
				playlistSong.setPlaylistId(Integer.parseInt(reader.readLine()));
				playlistSong.setSongId(Integer.parseInt(reader.readLine()));
				
				if (playlistSong.getPlaylistId()==playlistId) {
					songsIds.add(playlistSong.getSongId());
				}	

			}
		} catch (IOException |SecurityException e) {
			e.printStackTrace();
		}
		
		return songsIds;
	}
	
	public List<SongEntity> getSongsByPlaylistId(int playlistId, String destinationPath){
		List<Integer> ids =  getSongsIdByPlaylistId(playlistId);
		List<SongEntity> songs = new ArrayList<SongEntity>();
		Path path = Paths.get(destinationPath);

			try (BufferedReader reader = Files.newBufferedReader(path)) {
				String value = null;
				int i= 0;
				while ((value = reader.readLine()) != null) {
					SongEntity song =  new SongEntity();
					song.setId(Integer.parseInt(value));
					song.setTitle(reader.readLine());
					song.setArtistName(reader.readLine());
					song.setYear(Integer.parseInt(reader.readLine()));
					try {
						if (song.getId()==ids.get(i)) {
							songs.add(song);
							i++;
					}
					} catch (IndexOutOfBoundsException e) {
						break;
					}	
				}
			} catch (IOException |SecurityException e) {
				e.printStackTrace();
			}
			
		
 		
		return songs;
	}
	
	public void deleteSongFromPlaylist(int playlistId, int songId) {
		
		Path path = Paths.get(filePath);
		String fileCopy = filePath +".backup";
		Path fileCopyPath = Paths.get(fileCopy);
		PlaylistSongEntity playlistSong =  getSongToBeDeleted(playlistId, songId);
		try {	
			Files.copy(path, fileCopyPath, StandardCopyOption.REPLACE_EXISTING);
			BufferedReader reader = Files.newBufferedReader(fileCopyPath);
			BufferedWriter writer = Files.newBufferedWriter(path);
			

			String value = null;
			while ((value = reader.readLine()) != null) {
				if (value.equals(String.valueOf(playlistSong.getId()))) {
					
					reader.readLine();
					reader.readLine();
				} else {
					writer.write(value + System.lineSeparator());
					value = reader.readLine();
					writer.write(value + System.lineSeparator());
					value = reader.readLine();
					writer.write(value + System.lineSeparator());
				}
			}

			reader.close();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private PlaylistSongEntity getSongToBeDeleted(int playlistId, int songId) {
		Path path = Paths.get(filePath);
		PlaylistSongEntity playlistSong =  new PlaylistSongEntity();
 		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				playlistSong.setId(Integer.parseInt(value));
				playlistSong.setPlaylistId(Integer.parseInt(reader.readLine()));
				playlistSong.setSongId(Integer.parseInt(reader.readLine()));
				if (playlistSong.getPlaylistId()==playlistId & playlistSong.getSongId()==songId) {
					break;
				}	

			}
		} catch (IOException |SecurityException e) {
			e.printStackTrace();
		}
		
		return playlistSong;
	}
}
