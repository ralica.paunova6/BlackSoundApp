package com.app.common.repositories;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.app.common.entites.PlaylistEntity;
import com.app.common.entites.PlaylistSongEntity;
import com.app.common.entites.SharedPlaylistEntity;

public class PlaylistRepository extends BaseRepositoryImpl<PlaylistEntity>{

	public PlaylistRepository(String filePath, Class<PlaylistEntity> entityType) {
		super(filePath, entityType);
		// TODO Auto-generated constructor stub
	}

	public List<PlaylistEntity> getUserAllPlaylistsSongs (int userId){
		List<PlaylistEntity> playlists= new ArrayList<PlaylistEntity>();
		Path path = Paths.get(filePath);
 		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				PlaylistEntity playlist =  new PlaylistEntity();
				playlist.setId(Integer.parseInt(value));
				playlist.setName(reader.readLine());
				playlist.setCreator(Integer.parseInt(reader.readLine()));
				playlist.setDescription(reader.readLine());
				playlist.setPublic(Boolean.parseBoolean(reader.readLine()));
				if (playlist.getCreator()==userId) {
					playlists.add(playlist);
				}	

			}
		} catch (IOException |SecurityException e) {
			e.printStackTrace();
		}
		
		return playlists;
	}
	
	public List<PlaylistEntity> getPublicPlaylists (){
		List<PlaylistEntity> playlists= new ArrayList<PlaylistEntity>();
		Path path = Paths.get(filePath);
 		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				PlaylistEntity playlist =  new PlaylistEntity();
				playlist.setId(Integer.parseInt(value));
				playlist.setName(reader.readLine());
				playlist.setCreator(Integer.parseInt(reader.readLine()));
				playlist.setDescription(reader.readLine());
				playlist.setPublic(Boolean.parseBoolean(reader.readLine()));
				if (playlist.isPublic()==true) {
					playlists.add(playlist);
				}	

			}
		} catch (IOException |SecurityException e) {
			e.printStackTrace();
		}
		
		return playlists;
	}
	
	public void deleteCascade(int playlistId) {
		this.delete(playlistId);
		deletePlaylistConnections(playlistId, "PlaylistSongs.txt");
		deletePlaylistConnections(playlistId, "SharedPlaylists.txt");
	}
	
	public void deletePlaylistConnections(int playlistId, String destinationPath) {
		Path path = Paths.get(destinationPath);
		if (destinationPath.equals("PlaylistSongs.txt")) {
			PlaylistSongRepository repo = new PlaylistSongRepository(destinationPath, PlaylistSongEntity.class);
			try (BufferedReader reader = Files.newBufferedReader(path)) {
				String value = null;
				while ((value = reader.readLine()) != null) {
					PlaylistSongEntity playlistSong =  new PlaylistSongEntity();
					playlistSong.setId(Integer.parseInt(value));
					playlistSong.setPlaylistId(Integer.parseInt(reader.readLine()));
					playlistSong.setSongId(Integer.parseInt(reader.readLine()));
					if (playlistSong.getPlaylistId()==playlistId) {
						repo.delete(playlistSong.getId());
					}	
				}
					
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if (destinationPath.equals("SharedPlaylists.txt")) {
			SharedPlaylistRepository repo = new SharedPlaylistRepository(destinationPath, SharedPlaylistEntity.class);
			try (BufferedReader reader = Files.newBufferedReader(path)) {
				String value = null;
				while ((value = reader.readLine()) != null) {
					SharedPlaylistEntity sharedPlaylist =  new SharedPlaylistEntity();
					sharedPlaylist.setId(Integer.parseInt(value));
					sharedPlaylist.setPlaylistId(Integer.parseInt(reader.readLine()));
					sharedPlaylist.setUserId(Integer.parseInt(reader.readLine()));
					if (sharedPlaylist.getPlaylistId()==playlistId) {
						repo.delete(sharedPlaylist.getId());
					}	
				}
					
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
