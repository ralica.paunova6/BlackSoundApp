package com.app.common.repositories;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import com.app.common.entites.BaseEntity;

public abstract class BaseRepositoryImpl<T extends BaseEntity> implements BaseRepository<T>{

	public final Class<T> entityType;
	public final String filePath;
	
	public BaseRepositoryImpl(String filePath, Class<T> entityType) {
		this.entityType = entityType;
		this.filePath = filePath;
		
		if (!Paths.get(filePath).toFile().exists()) {
			try {
				Files.createFile(Paths.get(filePath));
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		}
	} 
	@Override
	public int getNextId() {

		Path path =  Paths.get(filePath);
		Field[] fields = entityType.getDeclaredFields();
		
		int id = 0;
		try(BufferedReader reader =  Files.newBufferedReader(path)){
			String value =  null;
			while ((value = reader.readLine())!=null) {
				id =  Integer.parseInt(value);
				for (int i = 0; i < fields.length; i++) {
					reader.readLine();
				}
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return id+1;
	}

	@Override
	public T getItem(int id) {
		Path path = Paths.get(filePath);
		Field[] fields = entityType.getDeclaredFields();

		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				if (value.equals(String.valueOf(id))) {
					// mapping
					T item = entityType.newInstance();
					item.setId(id);
					for (Field field : fields) {
						field.setAccessible(true);
						value = reader.readLine();
						getParsedValueOfField(field, value, item);
					}

					return item;
				} else {
					// skip
					for (int i = 0; i < fields.length; i++) {
						reader.readLine();
					}
				}
			}
		} catch (IOException | InstantiationException | IllegalAccessException | SecurityException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<T> getAll() {
		Path path = Paths.get(filePath);
		Field[] fields = entityType.getDeclaredFields();
		List<T>  items = new ArrayList<T>();
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
					// mapping
					T item = entityType.newInstance();
					item.setId(Integer.parseInt(value));
					for (Field field : fields) {
						value = reader.readLine();
						field.setAccessible(true);
						getParsedValueOfField(field, value, item);
						
					}
					items.add(item);
				
			} 
		}catch (IOException | InstantiationException | IllegalAccessException | SecurityException e) {
			e.printStackTrace();
		}
		
		return items;
	}

	@Override
	public void add(T item){
		Path path = Paths.get(filePath);
		StringBuilder content = new StringBuilder();
		try {
			item.setId(getNextId());
			content.append(item.getId()).append(System.lineSeparator());
			
			for (Field field : item.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					content.append(field.get(item)).append(System.lineSeparator());
				} catch (IllegalArgumentException | IllegalAccessException e) {
					
					e.printStackTrace();
				}
			}
			Files.write(path, content.toString().getBytes(), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void update(T item) {
		Path path =  Paths.get(filePath);
		String fileCopy = filePath + ".backup";
		Path fileCopyPath =  Paths.get(fileCopy);
		try {
			Files.copy(path, fileCopyPath, StandardCopyOption.REPLACE_EXISTING);
			BufferedReader reader = Files.newBufferedReader(fileCopyPath);
			BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
			Field[] fields = item.getClass().getDeclaredFields();
			String value = null;
			while ((value = reader.readLine())!=null) {
				String id = value;
				writer.write(id +System.lineSeparator());
				for (Field field : fields) {
					field.setAccessible(true);
					value = reader.readLine();
					if (id.equals(String.valueOf(item.getId()))) {
						writer.write(field.get(item).toString()+System.lineSeparator());
					}else {
						writer.write(value+System.lineSeparator());
					}
				}
			}
			reader.close();
			writer.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(int id) {
		Path path = Paths.get(filePath);
		String fileCopy = filePath +".backup";
		Path fileCopyPath = Paths.get(fileCopy);
		T item = getItem(id);
		try {	
			Files.copy(path, fileCopyPath, StandardCopyOption.REPLACE_EXISTING);
			BufferedReader reader = Files.newBufferedReader(fileCopyPath);
			BufferedWriter writer = Files.newBufferedWriter(path);
			Field[] fields = item.getClass().getDeclaredFields();

			String value = null;
			while ((value = reader.readLine()) != null) {
				if (value.equals(String.valueOf(id))) {
					
					for (int i = 0; i < fields.length; i++) {
						reader.readLine();
					}
				} else {
					writer.write(value + System.lineSeparator());
					for (Field field : fields) {
						field.setAccessible(true);
						value = reader.readLine();
						writer.write(value + System.lineSeparator());
					}
				}
			}

			reader.close();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void  getParsedValueOfField(Field field, String value, T item) {
		try {
			if (field.getType().equals(Boolean.class)) {
				field.set(item, Boolean.parseBoolean(value));
			}else if (field.getType().equals(Integer.class)) {
				field.set(item, Integer.parseInt(value));
			}
			else {
				field.set(item, value);
			}
			
		}catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
}
