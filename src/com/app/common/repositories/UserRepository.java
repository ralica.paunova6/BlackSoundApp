package com.app.common.repositories;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.app.common.entites.PlaylistEntity;
import com.app.common.entites.UserEntity;

public class UserRepository extends BaseRepositoryImpl<UserEntity>{

	public UserRepository(String filePath, Class<UserEntity> entityType) {
		super(filePath, entityType);
	}
	public UserEntity getByEmailPassword(String email, String password) {
		Path path = Paths.get(filePath);
		UserEntity user =  new UserEntity();
 		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				user.setId(Integer.parseInt(value));
				user.setEmail(reader.readLine());
				user.setPassword(reader.readLine());
				user.setIsAdministrator(Boolean.parseBoolean(reader.readLine()));
				user.setDisplayName(reader.readLine());
				if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
					return user;
				}	

			}
		} catch (IOException |SecurityException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void deleteCascade(int userId) {
		this.delete(userId);
		deletePlaylistByOwnerId(userId, "Playlist.txt");
		
	}
	
	private void deletePlaylistByOwnerId(int userId, String destinationPath) {
		PlaylistRepository playlistRepo = new PlaylistRepository(destinationPath, PlaylistEntity.class);
		Path path = Paths.get(destinationPath);
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				PlaylistEntity playlist =  new PlaylistEntity();
				playlist.setId(Integer.parseInt(value));
				playlist.setName(reader.readLine());
				playlist.setCreator(Integer.parseInt(reader.readLine()));
				playlist.setDescription(reader.readLine());
				playlist.setPublic(Boolean.parseBoolean(reader.readLine()));
				if (playlist.getCreator()==userId) {
					playlistRepo.deletePlaylistConnections(playlist.getId(), "PlaylistSongs.txt");
					playlistRepo.deletePlaylistConnections(playlist.getId(), "SharedPlaylists.txt");
					playlistRepo.delete(playlist.getId());
				}	
			}
				
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
}
