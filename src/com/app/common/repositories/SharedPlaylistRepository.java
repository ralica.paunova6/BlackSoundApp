package com.app.common.repositories;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.app.common.entites.PlaylistEntity;
import com.app.common.entites.PlaylistSongEntity;
import com.app.common.entites.SharedPlaylistEntity;
import com.app.common.entites.SongEntity;

public class SharedPlaylistRepository extends BaseRepositoryImpl<SharedPlaylistEntity> {

	public SharedPlaylistRepository(String filePath, Class<SharedPlaylistEntity> entityType) {
		super(filePath, entityType);
		
	}
	
	private List<Integer> getSharedPlaylistsIdByUserId(int userId) {
		List<Integer> playlistIds = new ArrayList<Integer>();
		Path path = Paths.get(filePath);
		
 		try (BufferedReader reader = Files.newBufferedReader(path)) {
			String value = null;
			while ((value = reader.readLine()) != null) {
				SharedPlaylistEntity sharedPlaylist =  new SharedPlaylistEntity();
				sharedPlaylist.setId(Integer.parseInt(value));
				sharedPlaylist.setPlaylistId(Integer.parseInt(reader.readLine()));
				sharedPlaylist.setUserId(Integer.parseInt(reader.readLine()));
				
				if (sharedPlaylist.getUserId()==userId) {
					playlistIds.add(sharedPlaylist.getPlaylistId());
				}	

			}
		} catch (IOException |SecurityException e) {
			e.printStackTrace();
		}
		
		return playlistIds;
		
	}
	public List<PlaylistEntity> getSharedPlaylistsByUserId(int userId, String destinationPath){
		List<Integer> ids =  getSharedPlaylistsIdByUserId(userId);
		List<PlaylistEntity> playlists = new ArrayList<PlaylistEntity>();
		Path path = Paths.get(destinationPath);

			try (BufferedReader reader = Files.newBufferedReader(path)) {
				String value = null;
				int i= 0;
				while ((value = reader.readLine()) != null) {
					PlaylistEntity playlist =  new PlaylistEntity();
					playlist.setId(Integer.parseInt(value));
					playlist.setName(reader.readLine());
					playlist.setCreator(Integer.parseInt(reader.readLine()));
					playlist.setDescription(reader.readLine());
					playlist.setPublic(Boolean.parseBoolean(reader.readLine()));
					try {
						if (!ids.isEmpty()) {
							if (playlist.getId()==ids.get(i)) {
								playlists.add(playlist);
								i++;
							}
						}
					} catch (IndexOutOfBoundsException e) {
						break;
					}
					
				}
			} catch (IOException |SecurityException e) {
				e.printStackTrace();
			}
			
		
 		
		return playlists;
	}
	
	

}
