package com.app.common.repositories;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.app.common.entites.PlaylistSongEntity;
import com.app.common.entites.SongEntity;

public class SongRepository extends BaseRepositoryImpl<SongEntity>{

	public SongRepository(String filePath, Class<SongEntity> entityType) {
		super(filePath, entityType);
		// TODO Auto-generated constructor stub
	}
	
	public void deleteCascade(int songId) {
		this.delete(songId);
		deleteSongConnections(songId, "PlaylistSongs.txt");
	}
	
	private void deleteSongConnections(int songId, String destinationPath) {
			PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository(destinationPath, PlaylistSongEntity.class);
			Path path = Paths.get(destinationPath);
			try (BufferedReader reader = Files.newBufferedReader(path)) {
				String value = null;
				while ((value = reader.readLine()) != null) {
					PlaylistSongEntity playlistSong =  new PlaylistSongEntity();
					playlistSong.setId(Integer.parseInt(value));
					playlistSong.setPlaylistId(Integer.parseInt(reader.readLine()));
					playlistSong.setSongId(Integer.parseInt(reader.readLine()));
					if (playlistSong.getSongId()==songId) {
						
						playlistSongRepo.delete(playlistSong.getId());
					}	
				}
					
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
}
