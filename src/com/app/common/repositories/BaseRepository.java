package com.app.common.repositories;


import java.util.List;

import com.app.common.entites.BaseEntity;

public interface BaseRepository<T extends BaseEntity> {
	
	
	int getNextId();
	T getItem(int id);
	List<T> getAll();
	void add(T item);
	void update(T item);
	void delete(int id);
	
}
