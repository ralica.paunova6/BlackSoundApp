package com.app.common.views;

import com.app.common.entites.UserEntity;
import com.app.common.repositories.UserRepository;

public class AuthService {
	private static AuthService instance =  null;
	private UserEntity  loggedUser =  null;
	
	private AuthService(){
		
	}
	
	public static AuthService getInstance() {
		
		if (AuthService.instance==null) {
			return AuthService.instance = new AuthService();
		}
		return AuthService.instance;
	}
	public UserEntity getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(String email, String password) {
		UserRepository userRepo=  new UserRepository("Users.txt", UserEntity.class);
		this.loggedUser = userRepo.getByEmailPassword(email, password);
	}
	
}
