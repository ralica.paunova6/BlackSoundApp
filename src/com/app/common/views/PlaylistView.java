package com.app.common.views;


import java.util.List;



import com.app.common.entites.PlaylistEntity;
import com.app.common.entites.PlaylistSongEntity;
import com.app.common.entites.SharedPlaylistEntity;
import com.app.common.entites.SongEntity;
import com.app.common.entites.UserEntity;
import com.app.common.repositories.BaseRepository;
import com.app.common.repositories.PlaylistRepository;
import com.app.common.repositories.PlaylistSongRepository;
import com.app.common.repositories.SharedPlaylistRepository;
import com.app.common.repositories.SongRepository;
import com.app.common.repositories.UserRepository;


public class PlaylistView extends BaseView<PlaylistEntity>{

	
	PlaylistRepository playlistRepo= new PlaylistRepository("Playlists.txt", PlaylistEntity.class);
	
	
	@Override
	public BaseRepository<PlaylistEntity> returnRepo() {
		this.entityType = PlaylistEntity.class;
		return playlistRepo;
	}
	
	@Override
	public MenuItems switchCustomMenuItems(int choice) {
		switch (choice) {
		case 8:
			return MenuItems.CUSTOM;
		default:
			return null;
		}
		
	}
	
	@Override
	public void listCustomsMenuItems() {
		System.out.println("8.Options");
		
	}
	
	@Override
	public void switchCustomRenderedMenuItems(MenuItems item) {
		switch (item) {
		case CUSTOM:
			initOptionsForPlaylist();
			break;

		default:
			break;
		}
		
	}
	
	public void initList() {
		
		
		listUserPlaylists(playlistRepo);
		
		listPublicPlaylists(playlistRepo);
		
		listSharedPlaylists();
		
	}
	
	private void listSharedPlaylists() {
		System.out.println("\nShared with me:");
		SharedPlaylistRepository sharedRepo = new SharedPlaylistRepository("SharedPlaylists.txt", SharedPlaylistEntity.class);
		if (!sharedRepo.getSharedPlaylistsByUserId(AuthService.getInstance().getLoggedUser().getId(), "Playlists.txt").isEmpty()) {
			for (PlaylistEntity playlist:sharedRepo.getSharedPlaylistsByUserId(AuthService.getInstance().getLoggedUser().getId(), "Playlists.txt")) {
				System.out.println(playlist.getId()+". Name: "+playlist.getName()+" Description: "+playlist.getDescription());
			}
			System.out.println("");
		}else {
			System.out.println("There are no shared playlists!\n");
		}
	}

	private void listUserPlaylists(PlaylistRepository playlistRepo) {
		System.out.println("My playlists:");
		for (PlaylistEntity playlist : playlistRepo.getUserAllPlaylistsSongs(AuthService.getInstance().getLoggedUser().getId())) {
			System.out.println(playlist.getId()+". Name: "+playlist.getName()+" Description: "+playlist.getDescription());
		}
	}
	
	private void listPublicPlaylists(PlaylistRepository playlistRepo) {
		UserEntity user = new UserEntity();
		UserRepository userRepo = new UserRepository("Users.txt", UserEntity.class);
		System.out.println("\nPublic playlists:");
		for (PlaylistEntity playlist : playlistRepo.getPublicPlaylists()) {
			user = userRepo.getItem(playlist.getCreator());
			System.out.println(playlist.getId()+". Name: "+playlist.getName()+" Description: "+playlist.getDescription()+" Owner: "+user.getDisplayName());
		}
	}
	
	public void initAdd(PlaylistEntity playlist) {
		while (true) {
			System.out.println("\nAdd data for the new playlist");
			System.out.println("Set name: ");
			playlist.setName(scanner.next());;
			System.out.println("Set description: ");
			playlist.setDescription(scanner.next());
			playlist.setCreator(AuthService.getInstance().getLoggedUser().getId());
			System.out.println("Is Public? (y/n)");
			if (scanner.next().equals("y")) {
				playlist.setPublic(true);break;
			}else if (scanner.next().equals("n")) {
				playlist.setPublic(false);break;
			}else {
				System.out.println("Invalid input! Try again.");
			}
		}

		
	}
	
	public PlaylistEntity initEdit(PlaylistEntity playlist) {
		
			
			System.out.println("\nEdit playlist. Select playlist do you want to edit.");
			listUserPlaylists(playlistRepo);
			
			System.out.println("\nPlaylist ID:");
			int playlistId = getValidNumberInput();
			playlist =playlistRepo.getItem(playlistId);
			
			while (true) {
				System.out.println("\nEdit Playlist");
				System.out.println("Playlist name: ");
				playlist.setName(scanner.next());
				System.out.println("Description: ");
				playlist.setDescription(scanner.next());
				System.out.println("Is Public? (y/n): ");
				if (scanner.next().equals("y")) {
					playlist.setPublic(true);
					return playlist;
				}else if (scanner.next().equals("n")) {
					playlist.setPublic(false);
					return playlist;
				}else {
					System.out.println("Invalid input! Try again.");
				}
		}
		
	}
	public void initDelete() {
		
		listUserPlaylists(playlistRepo);
		
		System.out.println("\nDelete Playlist:");
		System.out.println("Playlist ID:");
		int playlistId =  getValidNumberInput();
		playlistRepo.deleteCascade(playlistId);
	}
	
	
	public void initOptionsForPlaylist() {
		
		System.out.println("Select option for the playlist:");
		System.out.println("1.View songs from playlist");
		System.out.println("2.Add song to the playlist");
		System.out.println("3.Delete song from playlist");
		System.out.println("4.Share playlist with other user");
		System.out.println("5.Back");
		int choice = getValidNumberInput();
		
		switch (choice) {
		case 1: 
			listSongsFromPlaylist();
			break;
		case 2: 
			setSongToPlaylist();
			break;
		case 3:
			deleteSongFromPlaylist();
			break;
		case 4:
			sharePlaylist();
		case 5:
			getBack();
			break;

		default:
			break;
		}
		
		
	}
	public void listSongsFromPlaylist() {
		
		listUserPlaylists(playlistRepo);
		listPublicPlaylists(playlistRepo);
		listSharedPlaylists();
		
		//method start
		System.out.println("To list songs from specific playlist, select playlist ID from the list:");
		int playlistId = getValidNumberInput();
		
		PlaylistEntity playlist =  new PlaylistEntity();
		playlist =  playlistRepo.getItem(playlistId);
		System.out.println("\nThe chosen playlist is: ");
		System.out.println(playlist.getName()+" "+playlist.getDescription());
		//method end
		
		PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository("PlaylistSongs.txt", PlaylistSongEntity.class);
		List<SongEntity> songs = playlistSongRepo.getSongsByPlaylistId(playlist.getId(),"Songs.txt");
		if (songs.isEmpty()) {
			System.out.println("There are no songs in this playlist!");
		}else {
			for (SongEntity song : songs) {
				System.out.println("\tName: " +song.getTitle() + " Artis(s): "+song.getArtistName()+" Year: "+song.getYear());
			}
		}
		
		System.out.println("");
	}
	
	public void setSongToPlaylist() {
		
		listUserPlaylists(playlistRepo);
		listSharedPlaylists();
		
		System.out.println("To add a song to specific playlist, select playlist ID from the list:");
		int playlistId = getValidNumberInput();
		
		PlaylistEntity playlist =  new PlaylistEntity();
		playlist =  playlistRepo.getItem(playlistId);
		System.out.println("\nThe chosen playlist is: ");
		System.out.println(playlist.getName()+" "+playlist.getDescription());
		//method end 
		
		SongRepository songRepo =  new SongRepository("Songs.txt", SongEntity.class);
		System.out.println("List of songs:");
		for (SongEntity song : songRepo.getAll()) {
			System.out.println(song.getId()+". Title: "+song.getTitle()+" Artist(s): "+song.getArtistName()+" Year:"+song.getYear());
		}
		System.out.println("\nChoose a song from the list above:");
		int choice = getValidNumberInput();
		
		SongEntity song = new SongEntity();
		song = songRepo.getItem(choice);
		
	
		PlaylistSongEntity playlistSong =  new PlaylistSongEntity(playlist.getId(), song.getId());
		PlaylistSongRepository playlistSongRepo =  new PlaylistSongRepository("PlaylistSongs.txt", PlaylistSongEntity.class);
		playlistSongRepo.add(playlistSong);
		
		
	}
	
	public void deleteSongFromPlaylist() {
		
		listUserPlaylists(playlistRepo);
		listSharedPlaylists();
		
		//method start
		System.out.println("To delete songs from specific playlist, select playlist ID from the list:");
		int playlistId = getValidNumberInput();
		
		PlaylistEntity playlist =  new PlaylistEntity();
		playlist =  playlistRepo.getItem(playlistId);
		System.out.println("\nThe chosen playlist is: ");
		System.out.println(playlist.getName()+" "+playlist.getDescription());
		//method end 
		
		PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository("PlaylistSongs.txt", PlaylistSongEntity.class);
		List<SongEntity> songs = playlistSongRepo.getSongsByPlaylistId(playlist.getId(),"Songs.txt");
		for (SongEntity song : songs) {
			System.out.println("\t"+song.getId()+". Name: " +song.getTitle() + " Artis(s): "+song.getArtistName()+" Year: "+song.getYear());
		}
		System.out.println("\nTo delete song from this playlist select song: ");
		int songId = getValidNumberInput();
		
		playlistSongRepo.deleteSongFromPlaylist(playlistId, songId);
		
	}
	
	public void sharePlaylist() {
		System.out.println("Select playlist ID you want to share:\n ");
		
		listUserPlaylists(playlistRepo);
		
		int playlistId = getValidNumberInput();
		
		System.out.println("\nSelect user, with whom you want to share selected playlist.");
		UserRepository userRepository = new UserRepository("Users.txt", UserEntity.class);
		for (UserEntity user : userRepository.getAll()) {
			if (user.getId()==AuthService.getInstance().getLoggedUser().getId()) {
				continue;
			}
			System.out.println(user.getId()+". "+user.getDisplayName()+" Email: "+user.getEmail()+" "+(user.getIsAdministrator()==true?"Admin":"User"));
		}
		
		int userId =  getValidNumberInput();
		
		SharedPlaylistRepository sharePlaylistRepo = new SharedPlaylistRepository("SharedPlaylists.txt", SharedPlaylistEntity.class);
		sharePlaylistRepo.add(new SharedPlaylistEntity(playlistId, userId));
	}

	@Override
	public void initBack() {
		
	}
	
	public void getBack() {
		run();
	}
	
	
	
}

