package com.app.common.views;

import java.util.Scanner;

import com.app.common.entites.SongEntity;
import com.app.common.entites.UserEntity;


public class AdminView {
	Scanner scanner = new Scanner(System.in);
	
	public void  run() {
		
		if (AuthService.getInstance().getLoggedUser().getIsAdministrator()) {
			int choice = getMenuChoice();
				switch (choice) {
				case 1:
					BaseView<UserEntity>  userView = new UserView();
					userView.run();
					break;
				case 2:
					BaseView<SongEntity> songView =  new SongView();
					songView.run();
					break;
				default:
					getMenuChoice();
					break;
				}
		}
	}
	
	private int getMenuChoice() {
		int choice = 0;
		while (true) {
			System.out.println("Select menu");
			System.out.println("1. Users");
			System.out.println("2. Songs");
			String input = scanner.next();
			if (!input.isEmpty()&&input.matches("\\d")) {
				return choice=  Integer.parseInt(input);
			}else {
				System.out.println("Invalid input! Try again.\n");
			}
		
		}
	}
}
