package com.app.common.views;

import java.util.Scanner;

import com.app.common.entites.BaseEntity;
import com.app.common.repositories.BaseRepository;


public abstract class BaseView<T extends BaseEntity> {

	public enum MenuItems {ADD,EDIT,LIST,DELETE,CUSTOM,EXIT,LOGOUT,BACK};
	protected static Scanner scanner  = new Scanner(System.in);
	private BaseRepository<T> baseRepository ;
	public  Class<T> entityType;
	
	public abstract BaseRepository<T> returnRepo();
	public abstract void initAdd (T item);
	public abstract void initList();
	public abstract T initEdit(T item);
	public abstract void initDelete();
	public abstract void initBack();
	public abstract MenuItems switchCustomMenuItems(int choice);
	public abstract void switchCustomRenderedMenuItems(MenuItems item);
	public abstract void listCustomsMenuItems();
	
	
	public void  run() {
		
		while (true) {
			
			MenuItems item =  RenderMenu();
			switch (item) {
			case LIST:
				list();break;
			case ADD:
				add(); break;
			case EDIT:
				edit();break;
			case DELETE:
				delete();break;
			case BACK: 
				initBack();break;
			case LOGOUT:
				initLogout();break;
			case EXIT:
				return;
			default:
				switchCustomRenderedMenuItems(item);
			}
			
		}
		
	}
	
	public void list() {
		initList();
	}
	
	public void add() {
		
		try {
			baseRepository = returnRepo();
			T item = entityType.newInstance();
			initAdd (item);
			baseRepository.add(item);
		} catch (InstantiationException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		}
		
	}
	
	public void edit() {
		
		try {
			baseRepository = returnRepo();
			T item = entityType.newInstance();
			item = initEdit(item);
			baseRepository.update(item);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void delete() {
		initDelete();
	}
	
	private MenuItems  RenderMenu() {
		
		System.out.println("Select option.");
		System.out.println("1.List");
		System.out.println("2.Add");
		System.out.println("3.Edit");
		System.out.println("4.Delete");
		System.out.println("5.Back");
		System.out.println("6.Logout");
		System.out.println("7.Exit");
		listCustomsMenuItems();
		
		
		int choice = getValidNumberInput();
		 
		switch (choice) {
		
			case 1: return MenuItems.LIST;
			case 2: return MenuItems.ADD;
			case 3: return MenuItems.EDIT;
			case 4: return MenuItems.DELETE;
			case 5: return MenuItems.BACK;	
			case 6: return MenuItems.LOGOUT;
			case 7: return MenuItems.EXIT;
			default: return  switchCustomMenuItems(choice);
			
		}
		
		
	}
	
	public void initLogout() {
		LoginView loginView = new LoginView();
		loginView.logout();
		loginView.run();
	}
	
	
	protected int getValidNumberInput() {
		String choiceStr = scanner.next();
		choiceStr=choiceStr.trim();
		int choice=0;
		if (!choiceStr.isEmpty()&&choiceStr.matches("\\d")) {
			choice = Integer.parseInt(choiceStr);
			return choice;
		}else {
			System.out.println("\nPlease input a number\n");
			RenderMenu();
		}
		return 0;
	}
	
	
}
