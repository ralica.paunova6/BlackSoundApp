package com.app.common.views;


import com.app.common.entites.SongEntity;
import com.app.common.repositories.BaseRepository;
import com.app.common.repositories.SongRepository;

public class SongView extends BaseView<SongEntity>{

	SongRepository songRepository = new SongRepository("Songs.txt", SongEntity.class);
	
	@Override
	public BaseRepository<SongEntity> returnRepo() {
		this.entityType = SongEntity.class;
		return songRepository;
	}
	
	@Override
	public MenuItems switchCustomMenuItems(int choice) {
		
		return null;
	}
	
	@Override
	public void switchCustomRenderedMenuItems(MenuItems item) {
		
		
	}
	
	@Override
	public void listCustomsMenuItems() {
		
		
	}
	
	public void initList() {
		for (SongEntity song : songRepository.getAll()) {
			System.out.println(song.getId()+". Title: "+song.getTitle()+" Artist(s): "+song.getArtistName()+" Year:"+song.getYear());
		}
	}
	public void initAdd(SongEntity song) {
		
		while (true) {
			System.out.println("Add data for the new song");
			System.out.println("Set title: ");
			song.setTitle(scanner.next());;
			System.out.println("Set artist(s): ");
			song.setArtistName(scanner.next());;
			System.out.println("Set year: ");
			String input = scanner.next();
			if (input.matches("\\d{4}")) {
				song.setYear(Integer.parseInt(input)); 
				break;
			}else{
				System.out.println("Input valid year! try again.\n"); 
			}
		}
		
	}
	@Override
	public SongEntity initEdit(SongEntity song) {
		
			System.out.println("Edit song. Select song do you want to edit.");
			initList();
			System.out.println("Song ID:");
			int songId = getValidNumberInput();
			
			song =songRepository.getItem(songId);
			song.setId(songId);
			while (true) {
				System.out.println("Edit Song");
				System.out.println("Title: ");
				song.setTitle(scanner.next());
				System.out.println("Artist(s): ");
				song.setArtistName(scanner.next());
				System.out.println("Year: ");
				String input = scanner.next();
				if (input.matches("\\d{4}")) {
					song.setYear(Integer.parseInt(input)); 
					return song;
				}else{
					System.out.println("Input valid year! try again.\n"); 
				}	
		}
		
		
		
	}
	public void initDelete() {
		
		for (SongEntity song : songRepository.getAll()) {
			System.out.println(song.getId()+". Title: "+song.getTitle()+" Artist(s): "+song.getArtistName()+" Year:"+song.getYear());
		}
		System.out.println("\nDelete Song:");
		System.out.println("Song ID:");
		int songId =  Integer.parseInt(scanner.next());
		songRepository.deleteCascade(songId);
	}

	@Override
	public void initBack() {
		AdminView adminView = new AdminView();
		adminView.run();		
	}
	
}

