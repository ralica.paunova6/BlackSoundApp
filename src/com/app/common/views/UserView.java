package com.app.common.views;

import com.app.common.entites.UserEntity;
import com.app.common.repositories.BaseRepository;
import com.app.common.repositories.UserRepository;

public class UserView extends BaseView<UserEntity>{

	UserRepository userRepository = new UserRepository("Users.txt", UserEntity.class);
	@Override
	public BaseRepository<UserEntity> returnRepo() {
		this.entityType = UserEntity.class;
		return userRepository;
	}
	@Override
	public MenuItems switchCustomMenuItems(int choice) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void switchCustomRenderedMenuItems(MenuItems item) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void listCustomsMenuItems() {
		// TODO Auto-generated method stub
		
	}
	
	public void initList() {
		
		for (UserEntity user : userRepository.getAll()) {
			System.out.println(user.getId()+". "+user.getDisplayName()+" Email: "+user.getEmail()+" "+(user.getIsAdministrator()==true?"Admin":"User"));
		}
	}
	public void initAdd(UserEntity user) {
		
		while (true) {
			System.out.println("Add data for the new user");
			System.out.println("Set email: ");
			user.setEmail(scanner.next());
			//System.out.println("Input email!");continue;
			System.out.println("Set password: ");
			user.setPassword(scanner.next());
			//System.out.println("Input password!");continue;
			System.out.println("Set name: ");
			user.setDisplayName(scanner.next());
			//System.out.println("Input name!");continue;
			System.out.println("Is Admin? (y/n)");
			if (scanner.next().equals("y")) {
				user.setIsAdministrator(true); 
				break;
			}else if (scanner.next().equals("n")) {
				user.setIsAdministrator(false); 
				break;
			}else {
				System.out.println("Invalid input! Try again.");
			}
		}
		
		
	}
	public UserEntity initEdit(UserEntity user) {
		System.out.println("Edit user. Select user do you want to edit.");
		
		initList();
		System.out.println("\nUser ID:");
		int userId = getValidNumberInput();
		
		user =userRepository.getItem(userId);
		
		while (true) {
			System.out.println("Edit User");
			System.out.println("Email: ");
			user.setEmail(scanner.next());
			System.out.println("Password: ");
			user.setPassword(scanner.next());
			System.out.println("Name: ");
			user.setDisplayName(scanner.next());
			System.out.println("Is Admin? (y/n)");
			if (scanner.next().equals("y")) {
				user.setIsAdministrator(true);
				return user;
			}else if (scanner.next().equals("n")) {
				user.setIsAdministrator(false);
				return user;
			}else {
				System.out.println("Invalid input! Try again.");
			}
		}
		
		
		
		
		
	}
	public void initDelete() {
		for (UserEntity user : userRepository.getAll()) {
			System.out.println(user.getId()+". "+user.getDisplayName());
		}
		System.out.println("Delete User:");
		System.out.println("User ID:");
		int userId =  Integer.parseInt(scanner.nextLine());
		userRepository.deleteCascade(userId);
	}
	@Override
	public void initBack() {
		AdminView adminView = new AdminView();
		adminView.run();
		
	}
	
	
	
	
}

