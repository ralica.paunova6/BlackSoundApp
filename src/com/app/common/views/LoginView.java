package com.app.common.views;

import java.util.Scanner;

import com.app.common.entites.PlaylistEntity;

public class LoginView {
	public void run() {
		Scanner scanner = new Scanner(System.in);
		while (AuthService.getInstance().getLoggedUser()==null) {
			
			String email;
			String password;
			System.out.println("**********Welcome to BlackSound!*********** \n\nPlease login to continue :)");
			System.out.println("Enter Email: ");
			email = scanner.nextLine();
			System.out.println("Enter Password: ");
			password = scanner.nextLine();
			
			AuthService.getInstance().setLoggedUser(email, password);
			
		}
		
		if (!AuthService.getInstance().getLoggedUser().equals(null)) {
			System.out.println("\nHello, "+AuthService.getInstance().getLoggedUser().getDisplayName()+"\n");
			if (AuthService.getInstance().getLoggedUser().getIsAdministrator()) {
				AdminView adminView =  new AdminView();
				adminView.run();
			}else {
				
				BaseView<PlaylistEntity> playlistView =  new PlaylistView();
				playlistView.run();
			}
			
		}
	}
	
	public void logout() {
		AuthService.getInstance().setLoggedUser(null, null);
	}
}
