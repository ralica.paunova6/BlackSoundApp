package com.app.common.views;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.app.common.entites.PlaylistEntity;
import com.app.common.entites.PlaylistSongEntity;
import com.app.common.entites.SongEntity;
import com.app.common.entites.UserEntity;
import com.app.common.repositories.BaseRepository;
import com.app.common.repositories.UserRepository;

public class Main {

	public static void main(String[] args) {
		
	
		if (!Files.exists(Paths.get("Users.txt"), LinkOption.NOFOLLOW_LINKS)) {
			System.out.println(false);
			UserEntity admin = new UserEntity();
			admin.setEmail("admin@example.com");
			admin.setDisplayName("Admin");
			admin.setPassword("adminPass");
			admin.setIsAdministrator(true);
		
			BaseRepository<UserEntity> userRepo =  new UserRepository("Users.txt", UserEntity.class);
			userRepo.add(admin);
		}else {
			
			LoginView view = new LoginView();
			view.run();
		}
		
		
		
		
		
		
		
		
	}
	
	

}
