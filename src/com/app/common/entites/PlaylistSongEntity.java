package com.app.common.entites;



public class PlaylistSongEntity extends BaseEntity{
	private Integer playlistId;
	private Integer songId;
	
	public PlaylistSongEntity() {
		
	}

	public PlaylistSongEntity(int playlistId, int songId) {
		this.playlistId = playlistId;
		this.songId = songId;
	}
	public int getPlaylistId() {
		return playlistId;
	}
	public void setPlaylistId(int playlistId) {
		this.playlistId = playlistId;
	}
	
	public int getSongId() {
		return songId;
	}
	public void setSongId(int songId) {
		this.songId = songId;
	}
	
}
