package com.app.common.entites;


public class SongEntity extends BaseEntity{
	private String title;
	private String artistName;
	private Integer year;
	
	//private ArrayList<PlaylistEntity> playlists;
	
	public SongEntity() {

	}
	public SongEntity(String title, String artistName, int year) {
		this.title = title;
		this.artistName = artistName;
		this.year = year;
		//this.playlists = (ArrayList<PlaylistEntity>) playlists;
	}
	
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	
	/*public ArrayList<PlaylistEntity> getPlaylists() {
		return playlists;
	}
	public void setPlaylists(ArrayList<PlaylistEntity> playlists) {
		this.playlists = playlists;
	}*/


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
}
