package com.app.common.entites;

import com.app.common.views.AuthService;

public class PlaylistEntity extends BaseEntity{
	private String name;
	private Integer creator;
	//private ArrayList<SongEntity> songs;
	private String description;
	private Boolean isPublic;
	
	
	public PlaylistEntity() {
		
	}
	
	public PlaylistEntity(String name, String description, boolean isPublic) {
		this.name = name;
		this.creator = AuthService.getInstance().getLoggedUser().getId();
		//this.songs = (ArrayList<SongEntity>) songs;
		this.description = description;
		this.isPublic = isPublic;
	}
	/*public ArrayList<SongEntity> getSongs() {
		return songs;
	}
	public void setSongs(ArrayList<SongEntity> songs) {
		this.songs = songs;
	}*/
	
	
	public int getCreator() {
		return creator;
	}
	public void setCreator(int creator) {
		this.creator = creator;
	}
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
