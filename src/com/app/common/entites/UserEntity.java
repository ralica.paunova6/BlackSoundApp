package com.app.common.entites;

public class UserEntity extends BaseEntity {
	private String email;
	private String password;
	private Boolean isAdministrator;
	private String displayName;
	
	public UserEntity() {
		
	}
	public UserEntity(String email, String password, boolean isAdministrator, String displayName) {
		this.email = email;
		this.password =  password;
		this.isAdministrator = isAdministrator;
		this.displayName = displayName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public Boolean getIsAdministrator() {
		return isAdministrator;
	}
	public void setIsAdministrator(Boolean isAdministrator) {
		this.isAdministrator = isAdministrator;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
