package com.app.common.entites;

public class SharedPlaylistEntity extends BaseEntity {
	private Integer playlistId;
	private Integer userId;
	
	public SharedPlaylistEntity() {
		
	}
	
	public SharedPlaylistEntity(int playlistId, int userId) {
		this.playlistId = playlistId;
		this.userId = userId;
	}
	
	public Integer getPlaylistId() {
		return playlistId;
	}
	public void setPlaylistId(Integer playlistId) {
		this.playlistId = playlistId;
	}
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
